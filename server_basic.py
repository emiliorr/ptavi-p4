#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys

# Constantes. Puerto.
PORT = 6001


class EchoHandler(socketserver.BaseRequestHandler):
    """
    Echo server class
    """

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        data = self.request[0]
        sock = self.request[1]
        ip_client = self.client_address[0]
        port_client = self.client_address[1]
        received = data.decode('utf-8')
        print(ip_client, port_client, received)
        sock.sendto(received.upper().encode('utf-8'), self.client_address)


def main():
    # Listens at port PORT (my address)
    # and calls the EchoHandler class to manage the request
    PORT = int(sys.argv[1])
    try:
        serv = socketserver.UDPServer(('', PORT), EchoHandler)
        print(f"Lanzando servidor UDP de eco ({PORT})...")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()

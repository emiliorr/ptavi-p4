#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys


def main():
    try:
        SERVER = sys.argv[1]
        PORT = int(sys.argv[2])
        LINE = ''.join(sys.argv[3:len(sys.argv) - 1])
        EXPT = int(sys.argv[-1])
        if EXPT < 0:
            sys.exit('Usa: client_sip.py <ip> <puerto> register <sip_address> <expires_value')
    except ValueError:
        sys.exit('Usa: client_sip.py <ip> <puerto> register <sip_address> <expires_value')
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            print(f"Enviando a {SERVER}:{PORT}:", LINE)
            sip_line = f'{(LINE.split(" ")[0]).upper()} sip:{LINE.split(" ")[1]} SIP/2.0\r\n Expires: {EXPT}\r\n\r\n'
            my_socket.sendto(sip_line.encode('utf-8'), (SERVER, PORT))
            data = my_socket.recv(1024)
            print('Recibido: ', data.decode('utf-8'))
        print("Cliente terminado.")
    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()

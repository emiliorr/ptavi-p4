#!/usr/bin/python3
# -*- coding: utf-8 -*-

import contextlib
from io import StringIO
import socket
import unittest
from unittest.mock import patch, call, MagicMock, ANY

import client

messages = ['Hola', 'HOLA']
bmessages = [bytes(message, 'utf-8') for message in messages]


class TestSocket(unittest.TestCase):
    """Test how the socket used works"""

    @patch('client_basic.socket.socket')
    def test_creation(self, mock_socket):
        """Check that a socket is created"""

        client.main()
        mock_socket.assert_called_once_with(socket.AF_INET, socket.SOCK_DGRAM)

    @patch('client_basic.socket.socket.sendto')
    @patch('client_basic.socket.socket.recv')
    def test_sendrecv(self, mock_recv, mock_sendto):
        """Check that message is sent, and then something is received"""
        port = 6001
        mock_recv.return_value = bmessages[1]
        client.main()
        mock_sendto.assert_has_calls([call(bmessages[0], ('localhost', port)),
                                      call(bmessages[1], ('localhost', port))])
        mock_recv.assert_has_calls([])


class TestOutput(unittest.TestCase):
    """Test the output produced by the program in stdout"""

    @patch('client_basic.socket.socket.sendto')
    @patch('client_basic.socket.socket.recv')
    def test_sendrecv(self, mock_recv, mock_sendto):
        """Check that message is sent, then received, and properly printed"""

        port = 6001
        mock_recv.return_value = bmessages[1]

        stdout = StringIO()
        with contextlib.redirect_stdout(stdout):
            client.main()
            output = stdout.getvalue()
        mock_sendto.assert_has_calls([call(bmessages[0], ('localhost', port)),
                                      call(bmessages[1], ('localhost', port))])
        mock_recv.assert_has_calls([])
        self.assertEqual(f"Recibido: {messages[1]}\nRecibido: {messages[1]}\nCliente terminado.\n",
                         output)


if __name__ == '__main__':
    unittest.main()

#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Tests for server.py

Tests use mocking to avoid launching a real client.
"""

import contextlib
from io import StringIO
import os
import sys
import unittest
from unittest.mock import patch, ANY, MagicMock

from tests.common import parent_dir
import server

message = "Hola"
port = 6001
client = "127.0.0.1"
expected = f"""Listening: {port}...
Received: {message}.
"""


class TestSocket(unittest.TestCase):
    """Test how the server socket works"""

    @patch('server.socketserver.UDPServer')
    def test_creation(self, mock_udpserver):
        """Check that an object of class UDPServer is created"""

        server.main()
        mock_udpserver.assert_called_once_with(('', 6001), ANY)

    @patch('server.socketserver.UDPServer')
    def test_reception(self, mock_udpserver):
        """Check the handler object

        The handler object should be created every time a datagram is received.
        First, find the name of the class (by checking how UDPServer receives it
        when created), and then create a handler object (simulating the reception
        of a datagram), and check that the right answer is written back (in wfile)."""

        send_sock = MagicMock()

        server.main()
        handler_class = mock_udpserver.call_args.args[1]
        handler = handler_class((message.encode(), send_sock), ('client', 'port'), 'server')
        self.assertEqual((message.encode(), send_sock), handler.request)


class TestOutput(unittest.TestCase):
    """Test the output produced by the program in stdout"""

    @patch('server.socketserver.UDPServer')
    def test_reception(self, mock_udpserver):
        """Check the handler object

        Call the handler object (see technique used in TestSocket.test:reception),
        and check if the overall output produced by the program is as expected."""

        send_sock = MagicMock()

        stdout = StringIO()
        with contextlib.redirect_stdout(stdout):
            server.main()
            handler_class = mock_udpserver.call_args.args[1]
            handler = handler_class((message.encode(), send_sock), (client, port), 'server')
        output = stdout.getvalue()
        self.assertEqual((message.encode(),send_sock), handler.request)
        self.assertEqual(expected, output)


if __name__ == '__main__':
    unittest.main()
